#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main()
{
	struct hobby {
	
		int num;
		char *name;
	};
	
	typedef struct student {

		struct hobby *hob;
		int id;
		char *name;
		int age;
	}student;

	student *ptr = (student *) malloc(sizeof(student));
	ptr->hob = (struct hobby *) malloc(sizeof(struct hobby));
	ptr->id = 1234;
//	ptr->name = "Sankalp";
	char *name = "Sankalp";
	ptr->name = name; 
	ptr->age = 26;
	
	printf("id : %d\n", ptr->id);
	printf("name  : %s\n", ptr->name);
	printf("age : %d\n", ptr->age);
	
	char *hob_name = "kernel development";
	ptr->hob->num = 10;
	ptr->hob->name = hob_name;
	printf("hobby num : %d\n", ptr->hob->num);
	printf("name  : %d\n", *ptr->hob->name + 3);


	return 0;
}
