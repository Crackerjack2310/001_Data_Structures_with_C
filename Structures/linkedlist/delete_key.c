#include"declarations.h"
#include"header.h"

snode* delete_key(snode *start)
{
	int c,i=2;
	printf("\n Begin : %s \n",__func__);
	printf("\n Enter the node number to delete \n");
	scanf("%d",&c);	

	node *curr,*prev;

	prev=curr=start->first;

	if(c==curr->info)
        {               
       		start->first=curr->next;
		curr->next=NULL;
		free(curr);		
        	return start;
        }

	curr=curr->next;
		
	while(curr)
	{
		
		if(c==curr->info)	
		{
			prev->next=curr->next;
			curr->next=NULL;
			free(curr);
			return start;
		}
		
		prev=curr;
		curr=curr->next;
	}
	

	printf("Invalid key value entered..exiting!!\n");

	printf("\n End : %s \n",__func__);
	return start;	
}
