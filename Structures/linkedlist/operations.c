#include"declarations.h"
#include"header.h"

snode* operations(int ch,snode* start)
{

	switch(ch)
	{
		case 1:
			if(!start)
				start=creatlinkedlist();

			else
				printf("\n Linked List already created \n");
			break;		

		case 2: 
			if(!start)
                                printf("\nMissing start node..create one\n");
			else
				insert(start);
			break;

		case 3:
			if(!start)
				printf("\nNothing to delete...No list..Create one !!\n");
                   
			else if(start->first==NULL)	
                        	printf("\nOnly start node present..insert more...to delete some !!\n");
			else
				delete_ll(start);
			break;

		case 4: 
			if(!start)
				printf("\nNothing to sort\n");
                         
			else if(start->first==NULL)	
                        	printf("\nOnly start node present..insert more...to sort some !!\n");
			
			else if(start->first->next==NULL)	
                        	printf("\nOnly single node..already sorted !!\n");

			else
				sort(start);
			break;
	
		case 5:
			if(!start)
				printf("\nNothing to Display\n");
                        
			else if(start->first==NULL)	
                        	printf("\nOnly start node present..insert more..to show some !!\n");
	
			else
				display(start);
			break;
		
		default:
			exit(EXIT_FAILURE);

	}	
	return start;
}

