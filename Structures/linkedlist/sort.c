#include"declarations.h"
#include"header.h"

snode* sort(snode* start)
{
	printf("\n Begin : %s \n",__func__);
	int ch;
                
	ch=sortmenu();
        
	switch(ch)
        {
                case 1:
                	start=selection_sort(start);
			break;
                case 2:
                	start=bubble_sort(start);
                	break;

                case 3:
                        start=insertion_sort(start);	
                	break;

                default:
                	printf("\nInvalid choice..!! Reverting to Mainmenu \n");
        }

	printf("\n End : %s \n",__func__);
	return start;
}
