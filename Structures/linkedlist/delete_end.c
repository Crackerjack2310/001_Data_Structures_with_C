#include"declarations.h"
#include"header.h"

snode* delete_end(snode *start)
{
	printf("\n Begin : %s \n",__func__);

	node *last,*slast;
	slast=NULL;
	last=start->first;
	
	while(last->next)
	{
		slast=last;
		last=last->next;
	}

	if(slast)
		slast->next=NULL;
	
	else
		start->first=NULL;
		

	free(last);
	printf("\n End : %s \n",__func__);
	return start;

}
