#include"declarations.h"
#include"header.h"

snode* delete_beg(snode *start)
{
	printf("\n Begin : %s \n",__func__);
	
	node *temp;
	temp=start->first;
	start->first=start->first->next;
	temp->next=NULL;
	free(temp);		
	
	printf("\n End : %s \n",__func__);
	return start;
}
