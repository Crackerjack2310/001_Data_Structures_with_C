#include"header.h"
#include"declarations.h"

int decompress6(int efd,char* ma,int flag)
{
	printf("\n\n %s called\n",__func__);

	int dfd,i = 0,ret = 0;
	unsigned char ch,msb,lsb,new,rem,k = 0;		
	dfd=open("Decryption File",O_WRONLY|O_CREAT);
	printf("flag value : %d\n",flag);
	
	while(ret = read(efd,&ch,1))		
	{		


///////////////////////////////////////////////////////////////////////////////////  processing every 1st byte
		
		
		msb = ch >> 2;				// 1st character 2 bits rem in new
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);		// writing 1st character
	
		new = ch << 6;	
	 	new = new >> 2;	
	

////////////////////////////////////////////////////////////////////////////////////  processing every 2nd byte

	
		ret = read(efd,&ch,1);			// reading 2nd byte 

		if(!ret)
			break;
		rem = ch >> 4;		
		new = new | rem;		// getting our character back new + rem
		
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 2nd character	
  		
  		new = ch << 4;
		new = new >> 2;
  	
	
//////////////////////////////////////////////////////////////////////////////////////   processing every 3rd  byte		
		
		ret = read(efd,&ch,1);			// reading 3rd byte
		
		if(!ret)
			break;
		rem = ch >> 6;
		new = new | rem;		// getting our character back new + rem
		
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 3rd character
  	
  		msb = ch << 2;
		msb = msb >> 2;			// 4th character	
		i++;
                if( i <= flag )
		write(dfd,(ma+(int)msb),1);		// writing 4th character

	}	


//////////////////////////////////////////////////////////////////////////////////////   now looping things


	close(dfd);	// closing the Decryption File
	printf("\n\n %s   Ended : 6-bit Decryption Done\n",__func__);

}
