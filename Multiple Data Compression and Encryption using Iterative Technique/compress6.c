#include"header.h"
#include"declarations.h"

int compress6(int fd,char* ma)
{
	printf("\n\n %s called\n",__func__);
	
	int flag = 0,i = 0,d,efd,k = -1,ekey,j,ret = 0;	// variables used
	unsigned char byt = 0,c = 3,ch,msb,new,rem; // using unsigned char to avoid  '-' while shifting		
	efd = open("Encryption File",O_WRONLY|O_CREAT);	// file for Encryption
	
	lseek(fd,0,SEEK_SET);	// bring back the file descrp. to start address

	while(1)
	{	


//////////////////////////////////////////////////////////////////////////////////////// 1st Byte to be processed		
	
		k++;	// counts the current character no..under processing
		printf("\ncharacter at %d\n",k);		// displays that no.
		byt = byt ^ byt;		//xor with self  =  00000000  --- new byte begins
		ret = read(fd,&c,1);			//1st char read 
			
		if( c == '\n' || ret < 1) // to check occurrences of EOF -- break straight away -- nothing to write
			break;
													
		i = findIndex(c,ma);
		
		ch = (char) i;    //reduce 4 byte integer to 1 byte char..now only 8 bits
		printf("\nvalue read by ch : %c\n",ch);
		msb = ch << 2;
		printf("\nvalue read by msb: %d\n",msb);
		byt = byt | msb;
		flag++;
		k++;
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);			// 2nd char read

		if( c == '\n' || ret < 1)	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
              	
		i = findIndex(c,ma);
		
                ch = (char) i;    //reduce 4 byte integer to 1 byte char..now only 8 bits
		new = ch << 2;
		rem = new << 2;		// rem for pre-msb of next byte
		new = new >> 6;
		byt = byt | new;		// byte  =  msb + new 
		flag++;
		printf("\nvalue read by new: %d\n",new);

		write(efd,&byt,1);		//when a byt is complete --- write
    		printf("\n Encyption Value : %d \n\n\n",byt);	// Encrypted code


//////////////////////////////////////////////////////////////////////////////////////////////// 2nd Byte to be processed


		byt = byt ^ byt;		// 2nd byte begins --- rem + new
		byt = byt | rem;		// rem of last --pre msb-- 4 bits in byte as of now
		printf("\nvalue read by rem: %d\n",rem);	
		k++;	
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);			//3rd char read..place to msb
		
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		// when a byt is complete --- write
			break;
                }
               	
		i = findIndex(c,ma);
		
                ch = (char) i;    // reduce 4 byte integer to 1 byte char..now only 8 bits
               	new = ch << 2;
		rem = new << 4;		// rem for pre msb of next
		new = new >> 4;
		byt = byt | new;		// 2nd element written to  byte 
		flag++;
                printf("\nvalue read by new : %d\n",new);
		
		write(efd,&byt,1);			// 2nd byte written
                printf("\nEncryption value : %d\n\n\n",byt);	// Encrypted code


//////////////////////////////////////////////////////////////////////////////////////////////// 3rd Byte to be processed

	
		byt = byt ^ byt;       //3rd byte begins  rem + msb
                byt = byt | rem;               // pre msb-- rem of last
                printf("\nvalue read by rem: %d\n",rem);
                k++;
                printf("\ncharacter at %d\n",k);
                ret = read(fd,&c,1);                  //5th char read..place to msb

		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
              	
		i = findIndex(c,ma);
        	
                ch = (char) i;    //reduce 4 byte integer to 1 byte char..now only 8 bits
                msb = ch << 2;
		msb = msb >> 2;	
                byt = byt | msb;               // byte done
		flag++;
                printf("\nvalue read by new : %d\n",new);
								
		write(efd,&byt,1);                      // 3rd byte written
                printf("\nEncryption value : %d\n\n\n",byt); // Encrypted code
	
	}

	i = 0;
	k = 0;
	close(efd);	// Encryption file closed


/////////////////////////////////////////////////////////////////////////////////////  now writing Master Key to the file


	printf("flag value : %d\n",flag);	
	printf(" \n");	// This over here is preventing the creation of regular file, but how???
	ekey = open("Master Key",O_WRONLY|O_CREAT); // Master key file created
	
	if(ekey ==  -1)
	{
		perror("Master Key Failed");	// error incase Master key file creation failed
		exit(EXIT_FAILURE);
	}
	
	write(ekey,ma,strlen(ma));	// Master Key written

	ch = '#';
        write(ekey,&ch,1);      // adding the '#' char for detection of flag value
        char *flag_value;
        flag_value = (char*)malloc(sizeof(char)*10);
        snprintf(flag_value,10,"%d",flag);      //  flag's integer value as a string
        write(ekey,flag_value,(int)strlen(flag_value)); //writing it after '#' in Master Key


	close(ekey);	// Master Key file closed
	
	printf("\n\n %s   Ended : 6-bit Encryption Done\n",__func__);	

}						// Compression done..Lets check.!!
								
