#include"header.h"
#include"declarations.h"

int compress2(int fd,char* ma)
{
	printf("\n\n %s called\n",__func__);
	int flag = 0,i = 0,d,efd,k = 0,ekey,j,ret = 0;
	unsigned char byt = 0,c = 3,ch,bit;		
	efd=open("Encryption File",O_WRONLY|O_CREAT);
	
	lseek(fd,0,SEEK_SET);
	
	while(1)
	{	


/////////////////////////////////////////////////////////////////////////////////////////////// processing one byte


		byt = byt ^ byt;	//xor same = 00000000
		ret = read(fd,&c,1);		// read the odd no character
		if( c == '\n' || ret < 1)
			break;			// if no further to encrypt, break straight away, nothing to write
		
		i = findIndex(c,ma);
		ch = (char) i;	//reduce 4 byte integer to 1 byte char..now only 8 bits
		bit = ch << 6; // get 2 bits into first 2 bits of byt
		byt = byt | bit;
		flag++;	
		
		ret = read(fd,&c,1);			// reading the even no character
		if( c == '\n' || ret < 1)
		{	
			write(efd,&byt,1);	
			break;			// break, but first write the encrypted code, else we will lose it
		}
		
		i = findIndex(c,ma);
		ch = (char) i;	//reduce 4 byte integer to 1 byte char..now only 8 bits
		bit = ch << 6;
		bit = bit >> 2;		// get 2 bits to the 3rd and 4th bits of byt
		byt = byt | bit;
		flag++;	
		
		ret = read(fd,&c,1);			// reading the odd no character
		if( c == '\n' || ret < 1)
		{	
			write(efd,&byt,1);	
			break;			// break, but first write the encrypted code, else we will lose it
		}
		
		i = findIndex(c,ma);
		ch = (char) i;	//reduce 4 byte integer to 1 byte char..now only 8 bits
		bit = ch << 6;
		bit = bit >> 4;		// getting the 2 bits to 5th and 6th bits of byt
		byt = byt | bit;
		flag++;	
				
		ret = read(fd,&c,1);			// reading the even no character
		if( c == '\n' || ret < 1)
		{	
			write(efd,&byt,1);	
			break;			// break, but first write the encrypted code, else we will lose it
		}
		
		i = findIndex(c,ma);
		ch = (char) i;	//reduce 4 byte integer to 1 byte char..now only 8 bits
		bit = ch << 6;
		bit = bit >> 6;		// getting the 2 bits to 7th and 8th bits of byt
		byt = byt | bit;
		flag++;	
	
		write(efd,&byt,1); // byte written to file
    	//	printf("\n Encyption Value : %d \n\n\n",byt);


/////////////////////////////////////////////////////////////////////////////////////// now looping things


	}

	printf(" \n");		// this prevents creation of Master key as a regular file
	close(efd);		//  Encryption key file closes


//////////////////////////////////////////////////////////////////////////////////////// now writiing Master key 

	
	ekey = open("Master Key",O_WRONLY|O_CREAT);

	write(ekey,ma,(int)strlen(ma));

	ch = '#';
	write(ekey,&ch,1);	// adding the '#' char for detection of flag value
	char *flag_value;
	flag_value = (char*)malloc(sizeof(char)*10);
	snprintf(flag_value,10,"%d",flag);	//  flag's integer value as a string
	write(ekey,flag_value,(int)strlen(flag_value));	//writing it after '#' in Master Key

	close(ekey);		// Master key file closes
	
	printf("\n\n %s   Ended : 2-bit Encryption Done\n",__func__); // Encryption done ...lets check !!
}
