#include"header.h"
#include"declarations.h"

int compress3(int fd,char* ma)
{
	printf("\n\n %s called\n",__func__);// for rcs check
	
	int flag = 0,i = 0,d,efd,k = -1,ekey,j,ret = 0;	// variables used
	
	unsigned char byt = 0,c = 3,ch,msb,lsb,new,rem; // using unsigned char to avoid  '-' while shifting		
	efd = open("Encryption File",O_WRONLY|O_CREAT);	// file for Encryption
		
	lseek(fd,0,SEEK_SET);	// bring back the file descrp. to start address

	while(1)
	{	

//////////////////////////////////////////////////////////////////////////////////////// 1st Byte to be processed		
	
		k++;	// counts the current character no..under processing
		printf("\ncharacter at %d\n",k);		// displays that no.
		byt = byt ^ byt;		//xor same  =  00000000  byte begins
		ret = read(fd,&c,1);			//1st char ret = read 

		if( c == '\n' || ret < 1) // to check occurrences of EOF -- break straight away -- nothing to write
			break;
	
		i = findIndex(c,ma);
		sprintf(&ch,"%d",i);	//reduce 4 byte integer to 1 byte char..now only 8 bits
		msb = ch << 5;
		printf("\nvalue ret = read by msb: %d\n",msb);
		byt = byt | msb;
		flag++;
		k++;
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);			// 2nd char ret = read

		if( c == '\n' || ret < 1)	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
                else
                	i = findIndex(c,ma);
	
		sprintf(&ch,"%d",i);	
		ch = ch << 5;
		lsb = ch >> 3;
		byt = byt | lsb;		// byte  =  msb+lsb 2bits left
		flag++;
		printf("\nvalue ret = read by lsb: %d\n",lsb);
		k++;
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);	//3rd char ret = read
		
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
		else
                	i = findIndex(c,ma);
	
		sprintf(&ch,"%d",i);	// separate 2 bits for pre-msb  and 1 bit for rem
		new = ch << 5;
		rem = ch << 7;		//rem for next pre msb
		new = new >> 6;
		byt = byt | new;
		flag++;
		printf("\nvalue ret = read by new: %d\n",new);
		write(efd,&byt,1);		//when a byt is complete --- write
    		printf("\n Encyption Value : %d \n\n\n",byt);	// Encrypted code


//////////////////////////////////////////////////////////////////////////////////////////////// 2nd Byte to be processed


		byt = byt ^ byt;		// 2nd byte begins  rem+msb+lsb+new
		byt = byt | rem;		//1st byte of new byte --pre msb-- rem of last
		printf("\nvalue ret = read by rem: %d\n",rem);
		k++;	
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);			//4th char ret = read..place to msb
		
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
                else
               		i = findIndex(c,ma);
	
		sprintf(&ch,"%d",i);
               	msb = ch << 5;
		msb = msb >> 1;
		byt = byt | msb;		//2nd element written to  byte 4 bits left
		flag++;
                printf("\nvalue ret = read by msb: %d\n",msb);
		k++;	
		printf("\ncharacter at %d\n",k);	
		ret = read(fd,&c,1);			//5th char ret = read..place to lsb
	
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {	
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
                else
                	i = findIndex(c,ma);
        	
	        sprintf(&ch,"%d",i);
		lsb = ch << 5;
		lsb = lsb >> 4;
		byt = byt | lsb;	//3rd element of byte 1bit left for new of 6th char
		flag++;
                printf("\nvalue ret = read by lsb: %d\n",lsb);
		k++;	
                printf("\ncharacter at %d\n",k);
		ret = read(fd,&c,1);		// 6th char ret = read
	
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
		else
             		i = findIndex(c,ma);
	
		sprintf(&ch,"%d",i);
		new = ch << 5;
		new = new >> 7;
                printf("\nvalue ret = read by new: %d\n",new);
		rem = ch << 6;		//remaining 2 bits for next byte pre-msb
		byt = byt | new;	//2nd byte complete
		flag++;
		write(efd,&byt,1);			// 2nd byte written
                printf("\nEncryption value : %d\n\n\n",byt);	// Encrypted code


//////////////////////////////////////////////////////////////////////////////////////////////// 3rd Byte to be processed

	
		byt = byt ^ byt;       //3rd byte begins  rem+msb+lsb+new
                byt = byt | rem;               //1st byte of 3rd byte --pre msb-- rem of last
                printf("\nvalue ret = read by rem: %d\n",rem);
                k++;
                printf("\ncharacter at %d\n",k);
                ret = read(fd,&c,1);                  //7th char ret = read..place to msb

		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
                else
              		i = findIndex(c,ma);
        
	        sprintf(&ch,"%d",i);
                msb = ch << 5;
                msb = msb >> 2;
                byt  = byt | msb;               //2nd element written to  byte 3 bits left
		flag++;
                printf("\nvalue ret = read by msb: %d\n",msb);

                k++;
                printf("\ncharacter at %d\n",k);
                ret = read(fd,&c,1);                  //8th char ret = read..place to lsb
	
		if( c == '\n' || ret < 1 )	// to check mid - occurrences of EOF
                {	
			write(efd,&byt,1);		//when a byt is complete --- write
			break;
                }
                else
	        	i = findIndex(c,ma);
	
		sprintf(&ch,"%d",i);
                lsb = ch << 5;
                lsb = lsb >> 5;
                byt = byt | lsb;       //3rd element of byte 0bit left
		flag++;
                printf("\nvalue ret = read by lsb: %d\n",lsb);
                
		write(efd,&byt,1);                      // 3rd byte written
                printf("\nEncryption value : %d\n\n\n",byt); // Encrypted code
	
	}

	i = 0;k = 0;
	close(efd);	// Encryption file closed


//////////////////////////////////////////////////////////////////////////////////////// Now writing the Master Key entry


	printf(" \n");	
	ekey = open("Master Key",O_WRONLY|O_CREAT); // Master key file created
	
	if(ekey ==  -1)
	{
		perror("Master Key Failed");	// error incase Master key file creation failed
		exit(EXIT_FAILURE);
	}
	
	write(ekey,ma,strlen(ma));	// Master Key written
	
	ch = '#';
        write(ekey,&ch,1);      // adding the '#' char for detection of flag value
        char *flag_value;
        flag_value = (char*)malloc(sizeof(char)*10);
        snprintf(flag_value,10,"%d",flag);      //  flag's integer value as a string
        write(ekey,flag_value,(int)strlen(flag_value)); //writing it after '#' in Master Key


	close(ekey);	// Master Key file closed
	printf("\n\n %s   Ended : 3-bit Encryption Done\n",__func__);	
	

}						// Compression done..Lets check..!!									
