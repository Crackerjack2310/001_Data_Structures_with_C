#include"header.h"
#include"declarations.h"

int decompress7(int efd,char* ma,int flag)
{
	printf("\n\n %s called\n",__func__);

	int dfd,i = 0,ret=0;
	unsigned char ch,msb,lsb,new,rem,k = 0;		
	dfd=open("Decryption File",O_WRONLY|O_CREAT);
	
	while(ret = read(efd,&ch,1))		
	{		


///////////////////////////////////////////////////////////////////////////////////  processing every 1st byte
		
		
		msb = ch >> 1;				// 1st character 1 bit rem in new
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);		
	
		new = ch << 7;	
	 	new = new >> 1;	
	

////////////////////////////////////////////////////////////////////////////////////  processing every 2nd byte

	
		ret = read(efd,&ch,1);			// reading 2nd byte 

		if(!ret)
			break;
		rem = ch >> 2;		
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 2nd character	
  	
  		new = ch << 6;
		new = new >> 1;
  		

//////////////////////////////////////////////////////////////////////////////////////   processing every 3rd  byte		
		
		ret = read(efd,&ch,1);			// reading 3rd byte
		
		if(!ret)
			break;
		rem = ch >> 3;
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 3rd character
  	
  		new = ch << 5;
		new = new >> 1;			// new of 5th character	


///////////////////////////////////////////////////////////////////////////////////////  processing every 4th byte

	
		ret = read(efd,&ch,1);			// reading 4th byte

		if(!ret)
			break;
		rem = ch >> 4;		
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 4th character	
  	
  		new = ch << 4;
		new = new >> 1;	
			
  		
//////////////////////////////////////////////////////////////////////////////////////   processing every 5th byte		
		
		ret = read(efd,&ch,1);			// reading 5th byte
		
		if(!ret)
			break;
		rem = ch >> 5;
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 5th character
  	
  		new = ch << 3;
		new = new >> 1;


///////////////////////////////////////////////////////////////////////////////////////  processing every 6th byte

	
		ret = read(efd,&ch,1);			// reading 6th byte
	
		if(!ret)
			break;
		rem = ch >> 6;		
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 6th character	
  	
  		new = ch << 2;
		new = new >> 1;	
			
  		
//////////////////////////////////////////////////////////////////////////////////////   processing every 7th byte		
		
		ret = read(efd,&ch,1);			// reading 7th byte
		
		if(!ret)
			break;
		rem = ch >> 7;
		new = new | rem;		// getting our character back new + rem
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);		// writing 7th character
  		
  		msb = ch << 1;
		msb = msb >> 1;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);		// writing 8th character

	}	

//////////////////////////////////////////////////////////////////////////////////////   Re-looping things

	printf("\n\n %s   Ended : 7-bit Decryption Done\n",__func__);
		
	close(dfd);	// closing the Decryption File

}
