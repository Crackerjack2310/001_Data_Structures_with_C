#include"header.h"
#include"declarations.h"

int decompress4(int efd,char* ma,int flag)
{
	printf("\n\n %s called\n",__func__);

	int i=0,d,dfd,k=0;
	unsigned char c,ch,msb,lsb;		
	dfd=open("Decryption File",O_WRONLY|O_CREAT);
	printf("\nflag value = %d\n",flag);
	
	while(read(efd,&ch,1))
	{		

/////////////////////////////////////////////////////////////////////////////// processing a byte at a time

		msb=ch>>4;
	
		ch=ch<<4;
	 	lsb=ch>>4;
		printf("\n\n msb: %d    lsb: %d \n",msb,lsb);				

		i++;
		if( i <= flag )
			write(dfd,(ma+(int)msb),1);	//	write first character
		
		i++;
		if( i <= flag )
			write(dfd,(ma+(int)lsb),1);	// 	write second character
		
		printf("\n\n value of i: %d\n",i);	
	

//////////////////////////////////////////////////////////////////////////////// looping up things till EOL
	
	}	

	close(dfd);		// Decryption File closes
	printf("\n\n %s   Ended :4-bit Decryption Done\n",__func__);

}
