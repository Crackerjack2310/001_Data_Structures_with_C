#include"header.h"
#include"declarations.h"

int decompress3(int efd,char* ma,int flag)
{
	printf("\n\n %s called\n",__func__);

	int dfd,i = 0,ret = 0;
	unsigned char ch,msb,lsb,new,rem;		
	dfd=open("Decryption File",O_WRONLY|O_CREAT);
	
	while(ret = read(efd,&ch,1))		
	{		


///////////////////////////////////////////////////////////////////////////////////  processing every 1st byte

		
		msb = ch >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);
	
		lsb = ch << 3;
	 	lsb = lsb >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)lsb),1);
			
		new = ch << 6;
		new = new >> 5;
	

////////////////////////////////////////////////////////////////////////////////////  processing every 2nd byte

	
		ret = read(efd,&ch,1);

		if(!ret)
			break;
		rem = ch>>7;
		new = new | rem;		// getting our no. back
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);
  	
  		msb = ch << 1;
		msb = msb >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);
		
		lsb = ch << 4;
		lsb = lsb >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)lsb),1);
			
		new = ch << 7;
		new = new >> 5;	// leaving 2 spaces for rem to come
		
  		
//////////////////////////////////////////////////////////////////////////////////////   processing every 3rd  byte		
		
		
		ret = read(efd,&ch,1);
		
		if(!ret)
			break;
		rem = ch>>6;
		new = new | rem;		// getting our no. back
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)new),1);
  	
  		msb = ch << 2;
		msb = msb >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)msb),1);
		
		lsb = ch << 5;
		lsb = lsb >> 5;
		i++;
                if( i <= flag )
			write(dfd,(ma+(int)lsb),1);

	}	

	printf("\n\n %s   Ended : 3-bit Decryption Done\n",__func__);
		
	close(dfd);

}
