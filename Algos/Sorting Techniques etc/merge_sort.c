#include<stdio.h>
#include<stdlib.h>

int display_array(int*, int);
int get_array();
int* mergesort(int* , int, int);
int* merge(int*, int*, int, int);

int main()
{
	printf("%s : Begin\n", __func__);
	get_array();
	printf("%s : End\n", __func__);
	return 0;
}

int get_array()
{
	printf("%s : Begin\n", __func__);
	int i, temp, size; 
	int *ptr;
	printf("Enter the size of array\n");
	scanf("%d", &size);

	ptr=(int *)malloc(sizeof(int)*size);
	if(!ptr)
	{
		perror("Ptr Failed\n");
		exit(EXIT_FAILURE);
	}
	printf("Enter Array elements\n");
	
	for(i = 0;i < size; i++)
		scanf("%d",ptr + i);

	ptr = mergesort(ptr, 0, size - 1);		
	display_array(ptr, size);
	
	printf("%s : End\n", __func__);
	return 0;
}

int* mergesort(int *ptr, int p, int r)
{
	printf("%s : Begin\n", __func__);
	int i, length, ptr1_last, ptr2_last;
	length = r - p + 1; 

	if ( length == 1 )
		return ptr;
	
	ptr1_last = length/2 - 1;		//	10/2 = 5 - 1 = 4 ....01234     
	ptr2_last = length - ptr1_last - 1;	//	10 - 4  = 6 - 1 = 5 ....56789 
	
	printf("ptr1 length : %d\n", ptr1_last + 1);
	printf("ptr2 length : %d\n", ptr2_last);
	
	int *ptr1, *ptr2;
	ptr1 = (int*)malloc(sizeof(int)*(ptr1_last + 1));
	if (!ptr1)
	{
		perror("ptr1 failed");
		exit(1);
	}
	ptr2 = (int*)malloc(sizeof(int)*(ptr2_last));
	if (!ptr2)
	{
		perror("ptr2 failed");
		exit(1);
	}
	
	for (i = 0; i < length; i++)
	{
		if (i <= ptr1_last)
		{
			*(ptr1 + i) = *(ptr + i);
			printf("ptr1 at %d -->> %d\n", i, *(ptr1 + i));
		}
		else
		{
			*(ptr2 + i - ptr1_last - 1) = *(ptr + i);
			printf("ptr2 at %d -->> %d\n", i - ptr2_last, *(ptr2 + i - ptr2_last));
		}
	}
	printf("\v");	

	ptr1 = mergesort(ptr1, 0, ptr1_last);			// index
	ptr2 = mergesort(ptr2, 0, ptr2_last - 1);		// index
	ptr = merge(ptr1, ptr2, ptr1_last + 1, ptr2_last);		// size
	
	printf("%s : End\n", __func__);

	return ptr;
}

int* merge(int *ptr1, int *ptr2, int ptr1_last, int ptr2_last)
{	
	printf("%s : Begin\n", __func__);
	printf("merge\n");
	printf("*ptr1 = %d   *ptr2 = %d   ptr1_last = %d ptr3_last = %d\n", *ptr1, *ptr2, ptr1_last, ptr2_last);
	
	int *temp, i = 0, ptr1_check = 1, ptr2_check = 1;
	temp = malloc(sizeof(int)*(ptr1_last + ptr2_last));
		
	printf("temp size = %d\n", ptr1_last + ptr2_last);
	while(1)
	{
		printf("i = %d\n",i);
		if (*ptr1 < *ptr2)	
		{	
			*(temp + i) = *ptr1;		// place smaller in temp
			if (ptr1_check == ptr1_last)	// check if this was the last element --> collect all
			{
				while (1)	// loop till last of second pointer
				{
					i++;
					*(temp + i) = *ptr2;
					ptr2_check++;
					if (ptr2_check > ptr2_last)
						break;
					ptr2++;
				}
				break;
			}
			else
			{
				ptr1++;		// else increment
				ptr1_check++;
			}
		}
		else
		{
			*(temp + i) = *ptr2;		// place smaller in temp
			if (ptr2_check == ptr2_last)	// check if this was the last element --> collect all
			{
				while (1)	// loop till last of second pointer
				{
					i++;
					*(temp + i) = *ptr1;
					ptr1_check++;
					if (ptr1_check > ptr1_last)
						break;
					ptr1++;
				}
				break;
			}
			ptr2++;		// else increment
			ptr2_check++;
		}
	i++;
	}
	printf("%s : End\n", __func__);
	return temp;
}

int display_array(int *ptr, int size)
{
	printf("%s : Begin\n", __func__);
	printf("The array is as follows:\n");
	for(int i = 0; i < size; i++)
		printf("Sorted array[ %d ] -->> %d\n", i, *(ptr+i));
	printf("%s : End\n", __func__);
	return 0;
}
