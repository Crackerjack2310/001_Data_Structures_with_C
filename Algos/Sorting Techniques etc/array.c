#include<stdio.h>
#include<stdlib.h>

int display_array(int*);
int get_array();

int main()
{
	get_array();
		
	return 0;

}

int get_array()
{
	printf("Enter Array elements\n");
	int *ptr;
	ptr=(int *)malloc(sizeof(int)*10);
	
	for(int i=0;i<10;i++)
		scanf("%d",ptr+i);
	
	display_array(ptr);
}


int display_array(int *ptr)
{
	printf("The array is as follows:\n");
	//printf("ptr is %d and *ptr is %x\n",ptr,*ptr);	
	for(int i=0;i<10;i++)
		printf("base is %d ptr is %u value is -->> %d\n",i,ptr+i,*(ptr+i));
	
}
