#include<stdio.h>
#include<stdlib.h>
#include<string.h>


int display_array(int*);
int get_array();
int* mergesort(int *ptr, int p, int r);
int* merge(int *ptr1, int *ptr2);

int main()
{
	get_array();
	return 0;
}

int get_array()
{
	printf("Enter Array elements\n");
	int i, temp; 
	int *ptr;
	ptr=(int *)malloc(sizeof(int)*10);
	memset(ptr, 20, '\0');
	if(!ptr)
	{
		perror("Ptr Failed\n");
		exit(EXIT_FAILURE);
	}

	for(i = 0;i < 10; i++)
		scanf("%d",ptr + i);
	i = 0;
	while (ptr)
	{
		i++;
		ptr++;
	printf("length = %d\n", i);	
	}
	//display_array(ptr);
	
	return 0;
}

int display_array(int *ptr)
{
	printf("The array is as follows:\n");
	for(int i=0;i<10;i++)
		printf("-->> %d\n", *(ptr+i));
	return 0;
}
