#include<stdio.h>
#include<stdlib.h>

int get_array();

int main()
{
	get_array();
	return 0;
}

int get_array()
{
	printf("Enter Array elements in ascending order only \n");
	int i,*ptr,temp,key;
	ptr=(int *)malloc(sizeof(int)*10);
	
	if(!ptr)
	{
		perror("Ptr Failed");
		exit(EXIT_FAILURE);
	}

	for(i=0; i<10; i++)
		scanf("%d",ptr+i);
	printf("Enter key to be found\n");
	scanf("%d",&key);

	for(i=0; i<10; i++)
		printf("%d\n",*(ptr+i));

	int low = 0, high = 9, result;
			
	result = low + (((high - low) * (key - *ptr)) / (*(ptr + high) - *ptr));
	printf("Element found at %d\n",result);	
	return 0;
}
