#include<stdio.h>
#include<stdlib.h>

int display_array(int*, int);
int get_array();
int* quicksort(int* , int, int);
int* merge(int*, int*, int, int);

int main()
{
	printf("%s : Begin\n", __func__);
	get_array();
	printf("%s : End\n", __func__);
	return 0;
}

int get_array()
{
	printf("%s : Begin\n", __func__);
	int i, temp, size; 
	int *ptr;
	printf("Enter the size of array\n");
	scanf("%d", &size);

	ptr=(int *)malloc(sizeof(int)*size);
	if(!ptr)
	{
		perror("Ptr Failed\n");
		exit(EXIT_FAILURE);
	}
	printf("Enter Array elements\n");
	
	for(i = 0;i < size; i++)
		scanf("%d",ptr + i);	

	ptr = quicksort(ptr, 0, size - 1);		
	display_array(ptr, size);
	
	printf("%s : End\n", __func__);
	return 0;
}

int* quicksort(int *ptr, int beg, int end)
{
	printf("%s : Begin\n", __func__);

	if (beg >= end)			/* Stop condition for recursion */
		return ptr;

	int i, fence, temp, flag = 0, beg_copy, pivot, last;
	
	pivot = *(ptr + beg);		/* reference value from array */
	beg_copy = beg;	/* saving value of beg for last swap */
	fence = beg + 1;
	beg++;				/* leaving out the pivot element */

	while(beg <= end)
	{
		if ( (*(ptr + beg) > pivot) && (flag == 0) )		/* stop condition for recursion */
		{
			fence = beg;		/* getting the boundary element index */
			flag = 1;		/* setting a flag for further procedure */
		}

		/* else we begin with the procedure for quicksort */
		
		if ( (*(ptr + beg) < pivot) && (flag == 1) )
		{
			temp = *(ptr + beg);
			*(ptr + beg) = *(ptr + fence);
			*(ptr + fence)  = temp;
			fence++;		/* boundary moves head to point to first larger */
		}	
		beg++;
	}
	
	if (flag)
	{
		fence--;				/* index for the last smaller */
		*(ptr + beg_copy) = *(ptr + fence);	/* swapping pivot value with last smaller*/
		*(ptr + fence)  = pivot;		/* pivot now in some mid indices */
		last = fence;
	}
	else						/* if all elements were smaller than pivot */
	{
		*(ptr + beg_copy) = *(ptr + end);	/* swapping pivot value with last value*/
		*(ptr + end)  = pivot;		/* pivot now at last */
		last = end;
	}	
	
	quicksort(ptr, beg_copy, last - 1);
	quicksort(ptr, last + 1, end);
	
	printf("%s : End\n", __func__);
	return ptr;
}

int display_array(int *ptr, int size)
{
	printf("%s : Begin\n", __func__);
	printf("The array is as follows:\n\v");
	for(int i = 0; i < size; i++)
		printf("Sorted array[ %d ] -->> %d\n", i, *(ptr+i));
	printf("\n%s : End\n", __func__);
	return 0;
}
