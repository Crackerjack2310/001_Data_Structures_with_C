typedef struct node
{
	int info;
	struct node *next;
}node;

typedef struct snode
{
	int memusage;
	struct node *first;
}snode;

extern int info;

int mainmenu();

snode* operations(int,snode*);
snode* creatlinkedlist();
node* creatnode();

snode* insert(snode*);
int insertmenu();
snode* insert_beg(snode*);
snode* insert_end(snode*);
snode* insert_n(snode*);
snode* insert_key(snode*);

snode* delete_ll(snode*);
int deletemenu();
snode* delete_beg(snode*);
snode* delete_end(snode*);
snode* delete_n(snode*);
snode* delete_key(snode*);

int display(snode*);

snode* sort(snode*);
int sortmenu();
snode* selection_sort(snode*);
snode* bubble_sort(snode*);
snode* insertion_sort(snode*);



