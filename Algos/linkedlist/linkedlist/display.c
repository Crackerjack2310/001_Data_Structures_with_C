#include"declarations.h"
#include"header.h"

int display(snode *start)
{
	printf("\n Begin : %s \n",__func__);
	
	printf("\nLinked list: \n");
	int i=1;	
	node *temp;
	temp=start->first;
	
	printf("\n\nNode %d : %d\n",i,temp->info);
	while(temp->next!=NULL)
	{	
		i++;
		temp=temp->next;
		printf("Node %d : %d\n",i,temp->info);
	}
	
	printf("\n\n End : %s \n",__func__);
	
}
