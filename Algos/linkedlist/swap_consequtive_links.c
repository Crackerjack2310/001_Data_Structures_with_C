#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy curr data
	new_node->next = (*head);		// push the curr node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n",__func__);
}

void display_list(Node *head)
{
	printf("%s : Begin\n\n",__func__);
	while (head)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n",__func__);
}
	
Node* swap(Node **head)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *prev = (*head), *curr, *last;			// prev = address of 1st
	curr = last = NULL;
	last = malloc(sizeof(Node));
	
	if ((prev == NULL) || (prev->next == NULL))					// if 2nd node exists
	{
		printf("Nothing to swap !!\n");
		return (*head);
	}
	(*head) = prev->next;				// *head = address of 2nd				

	while (prev->next)				// prev = first node, till we have a second node
	{	
		curr = prev->next;			// save 2nd element
		prev->next = curr->next;		// now 1st points to 3rd
		curr->next = prev;			// now 2nd points to 1st	
		last->next = curr;			// last element fromprevious iteration points to curr 
		last = prev;				// preserving the address of now 2nd element
		prev = prev->next;			// now prev points to 3rd. curr 1st
		if (!prev)				// update prev to point to 3rd element
			break;				// prev = 3/
	}				
	printf("%s : End\n\v",__func__);
	return (*head);
}	

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *head = NULL;
	printf("Original linked list : \n");
	push(&head, 20);
	push(&head, 787);
	push(&head, 734);
	push(&head, 42);
	push(&head, 2);
	push(&head, 7);
	push(&head, 45);
	push(&head, 7);
	push(&head, 23);
	push(&head, 234);
	display_list(head);
	head = swap(&head);
	printf("\nNew linked list with swapped pairs : \n");
	display_list(head);

	printf("%s : End\n",__func__);
	return 0;
}
