#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int display_list(Node *);
Node* reverse_list(Node *);
Node* address;				// global variable for storing  address of last exited node, to avoid calculation of length in advance

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *temp = NULL, *start = (Node *)malloc(sizeof(Node));
	get_list(start);				// input list
	printf("Now displaying list\n");
	display_list(start);				// display list
	printf("Now displaying reversed list\n");
	temp = reverse_list(start);
	start->next=NULL;
	display_list(temp);				// display list
	printf("%s : End\n",__func__);
	return 0;
}

int get_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	first->info = 0;
	for (i = 1; i < 10; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = NULL;
	printf("%s : End\n",__func__);
	return 0;
}

int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	int i = 0;
	while (first)
	{
		printf("Node %d -->>: %d\n", i, first->info);
		first = first->next;
		i++;
	}		
	printf("%s : End\n",__func__);
	return 0;
}

Node* reverse_list(Node *rest)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *new;
		
	if (!rest->next)
	{
		address = rest;			// address of node from where we return, hence no need for calculating length in advance
		return rest;
	}
	new = reverse_list(rest->next);
	address->next = rest;
	address = rest;				// address of node from where we return, hence no need for calculating length in advance
	printf("%s : End\n",__func__);
	return new;
}
