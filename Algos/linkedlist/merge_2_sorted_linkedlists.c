#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy curr data
	new_node->next = (*head);		// push the curr node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n",__func__);
}
void display_list(Node *head)
{
	printf("%s : Begin\n\n",__func__);
	while (head != NULL)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n",__func__);
}	
Node* compare(Node* element, Node* list)
{
	printf("%s : Begin\n",__func__);
	Node* slast = NULL;
	while(element->data >= list->data)
	{
		slast = list;
		list = list->next;
		if (!list)
		{
			printf("\n\n%s : End\n",__func__);
			return slast;
		}
	}
	printf("\n\n%s : End\n",__func__);
	return slast;
}
Node* merge(Node **head1, Node **head2)		// as address of current
{
	printf("%s : Begin\n",__func__);

	Node *first = (*head1), *second = (*head2), *temp;
	if ((*head1) == NULL)
		return (*head2);
	if ((*head2) == NULL)
		return (*head1);
	if (second->data < first->data)
	{
		(*head1) = second;
		second = second->next;		// new list
		(*head1)->next = first;		
	}
							       // 15>>26>>65>>101>>165>>202>>NULL
							       // 30>>43>>47>>86>>123>>300>>NULL
	while (second && first)
	{	
		first = compare(second, first);	
		temp = first->next;
		first->next = second;
		first->next->next = temp;
		second = second->next;			// 43
		first = first->next;			// 65	
	}	
	printf("%s : End\n",__func__);
	return (*head1);
}

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *head1 = NULL, *head2 = NULL;
	push(&head1, 902);
	push(&head1, 782);
	push(&head1, 734);
	push(&head1, 422);
	push(&head1, 200);
	push(&head1, 97);
	push(&head1, 41);
	push(&head1, 34);
	push(&head1, 25);
	push(&head1, 23);

	push(&head2, 1000);
	push(&head2, 900);
	push(&head2, 723);
	push(&head2, 492);
	push(&head2, 456);
	push(&head2, 398);
	push(&head2, 324);
	push(&head2, 90);
	push(&head2, 34);
	push(&head2, 26);
	
	printf("Original linked lists : \n");
	display_list(head1);
	display_list(head2);
	head1 = merge(&head1, &head2);
	printf("New sorted linked list : \n");
	display_list(head1);
	printf("%s : End\n",__func__);
	return 0;
}
