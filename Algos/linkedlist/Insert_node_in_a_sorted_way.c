#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy curr data
	new_node->next = (*head);		// push the curr node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n",__func__);
}

void display_list(Node *head)
{
	printf("%s : Begin\n\n",__func__);
	while (head)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n",__func__);
}	

void insertnode(Node **head, Node *newnode)		// as address of current
{
	printf("%s : Begin\n",__func__);
	Node *current = (*head); 
	
	if (current == NULL || (newnode->data <= current->data))  // Insertion at beginning condition
	{
		newnode->next = current;
		(*head) = newnode;		// no need to return as we have address of head node as argument
	}
	else						// Insertion after first node
	{	
		while (current->next && (newnode->data >= current->next->data))
			current = current->next;		// traverse till no node or node data > newnode data
		newnode->next = current->next;		
		current->next = newnode;
	}
	printf("%s : End\n",__func__);
}

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *head = NULL, *newnode = (Node*)malloc(sizeof(Node));
	push(&head, 902);
	push(&head, 782);
	push(&head, 734);
	push(&head, 422);
	push(&head, 200);
	push(&head, 97);
	push(&head, 41);
	push(&head, 34);
	push(&head, 25);
	push(&head, 23);
	printf("Original linked list : \n");
	display_list(head);
	printf("Enter new node data : ");
	scanf("%d", &newnode->data);
	insertnode(&head, newnode);
	display_list(head);
	printf("%s : End\n",__func__);
	return 0;
}
