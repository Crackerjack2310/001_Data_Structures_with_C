#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy new data
	new_node->next = (*head);		// push the new node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n",__func__);
}

void display_list(Node *head)
{
	printf("%s : Begin\n\n",__func__);
	while (head)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n",__func__);
}

Node* remove_duplicates(Node **head)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *temp = (*head), *new;

	if (temp == NULL)
		return (*head);
	while (temp->next)
	{
		if (temp->data == temp->next->data)
		{
			new = temp->next->next;
			free(temp->next);
			temp->next = new;
		}
		else
			temp = temp->next;
	}
	printf("%s : End\n",__func__);
		return (*head);
}

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *head = NULL;
	push(&head, 100);
	push(&head, 67);
	push(&head, 67);
	push(&head, 67);
	push(&head, 14);
	push(&head, 13);
	push(&head, 13);
	push(&head, 1);
	printf("Original linked list : \n");
	display_list(head);
	head = remove_duplicates(&head);
	printf("\nNew linked list without duplicate elements : \n");
	display_list(head);

	printf("%s : End\n",__func__);
	return 0;
}
