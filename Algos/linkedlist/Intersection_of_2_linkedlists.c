#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy curr data
	new_node->next = (*head);		// push the curr node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n",__func__);
}

void display_list(Node *head)
{
	printf("%s : Begin\n\n",__func__);
	while (head)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n",__func__);
}
	
Node* swap(Node **head1, Node **head2)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *list1 =(*head1), *list2 = (*head2);		
	int count1 = 0, count2 = 0, diff = 0;

	while (list1)
	{
		list1 = list1->next;
		count1++;
	}
	while (list2)
	{
		list2 = list2->next;
		count2++;
	}
	diff = (count1 > count2) ? (count1 - count2) : (count2 - count1); // difference 
	
	list1 = (count1 > count2) ? (*head1) : (*head2);	// bigger list
	list2 = (count2 < count1) ? (*head2) : (*head1);	// smaller list
	
	printf("Difference in length : %d\n", diff);
	while (diff--)
	{
		list1 = list1->next;
	}

	while (list1)
	{
		if (list1->next == list2->next)
		{
			printf("intersection at : %d\n", list1->next->data);
			return 0;
		}
		list1 = list1->next;
		list2 = list2->next;
	}
	printf("No point of Intersection\n");
	printf("%s : End\n",__func__);
}	

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *head1 = NULL;
	Node *head2 = NULL;
	printf("Original linked list : \n");
	push(&head1, 20);
	push(&head1, 787);
	push(&head1, 734);
	push(&head1, 42);
	push(&head1, 2);
	push(&head1, 97);
	push(&head1, 45);
	push(&head1, 7);
	push(&head1, 23);
	push(&head1, 234);

	push(&head2, 33);
	push(&head2, 77);
	push(&head2, 22);
	push(&head2, 99);
	
	head2->next->next->next->next = head1->next->next;
	display_list(head1);
	display_list(head2);
	swap(&head1, &head2);

	printf("%s : End\n",__func__);
	return 0;
}
