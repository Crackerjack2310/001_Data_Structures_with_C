#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int display_list(Node *);
Node* reverse_list(Node *);

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *start;
	start = (Node *)malloc(sizeof(Node));

	get_list(start);			// input list
	display_list(start);			// display list
	start = reverse_list(start);
	printf("Now displaying reversed list\n");
	display_list(start);			// display list

	printf("%s : End\n",__func__);
	return 0;
}

int get_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	first->info = 0;
	for (i = 1; i < 25; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = NULL;
	printf("%s : End\n",__func__);
	return 0;
}

int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	int i = 0;
	while (first)
	{
		printf("Node %d -->>: %d\n", i, first->info);
		first = first->next;
		i++;
	}		
	printf("%s : End\n",__func__);
	return 0;
}

Node* reverse_list(Node *start)
{
	printf("%s : Begin\n",__func__);
	Node *last, *tlast, *temp;
	last = tlast = start;
	int node = 1, count, var;

	while (last->next)
	{
		last = last->next;		// goto last Node
		node++;				// count no of nodes
	}
	temp = last;				// for returning to main
	count = node - 3;			// targetting third last node
	while (count > 0)			// if less than 3 elements then tlast is same as start node	
	{
		tlast = tlast->next;		// third last node
		count--;
	}
	count = node - 3;
	var = count;
	while (count >= 0)
	{
		last->next = tlast->next;	// reversing
		tlast = start;
		var = count - 1;
		while (var > 0)			// stop on the second last node	
		{	
			tlast = tlast->next;
			var--;
		}
		last = last->next;
		count--;
	}
	last->next = tlast;		// reversing last node, initially first
	tlast->next = NULL;		// put start->next = NULL

	printf("%s : End\n",__func__);
	return temp;
}
