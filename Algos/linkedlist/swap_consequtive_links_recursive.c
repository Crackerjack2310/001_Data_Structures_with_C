#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int data;
}Node;

void push(Node **head, int data)		// *head = NULL   head = address of head node
{
	printf("%s : Begin\n",__func__);
	Node *new_node = (Node*)malloc(sizeof(Node));
	
	new_node->data = data; 			// copy curr data
	new_node->next = (*head);		// push the curr node to point to head 
       	(*head) = new_node;			// update the head value for next call to the function		
	
	printf("%s : End\n", __func__);
}

void display_list(Node *head)
{
	printf("%s : Begin\n\n", __func__);
	while (head)
	{
		printf("-->> %d ", head->data);
		head = head->next;
	}		
	printf("\n\n%s : End\n", __func__);
}
	
Node* swap(Node *node)		// as address of start
{
	printf("%s : Begin\n", __func__);
	if (node == NULL || node->next == NULL)
		return node;
	Node *curr, *prev = node;
	curr = prev->next;			// last 
	prev->next = swap(node->next->next);	
	curr->next = prev;
	return curr;
	printf("%s : End\n\v", __func__);
}	
int main()
{
	printf("%s : Begin\n", __func__);	
	Node *head = NULL;
	printf("Original linked list : \n");
	push(&head, 11);
	push(&head, 10);
	push(&head, 9);
	push(&head, 8);
	push(&head, 7);
	push(&head, 6);
	push(&head, 5);
	push(&head, 4);
	push(&head, 3);
	push(&head, 2);
	push(&head, 1);
	display_list(head);
	head = swap(head);
	printf("\nNew linked list with swapped pairs : \n");
	display_list(head);

	printf("%s : End\n",__func__);
	return 0;
}
