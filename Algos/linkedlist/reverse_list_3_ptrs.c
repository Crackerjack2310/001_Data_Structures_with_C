#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int display_list(Node *);
Node* reverse_list(Node **);

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *start;
	start = (Node *)malloc(sizeof(Node));

	get_list(start);				// input list
	printf("Now displaying list\n");
	display_list(start);				// display list
	start = reverse_list(&start);
	printf("Now displaying reversed list\n");
	display_list(start);				// display list

	printf("%s : End\n",__func__);
	return 0;
}

int get_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	first->info = 0;
	for (i = 1; i < 10; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = NULL;
	printf("%s : End\n",__func__);
	return 0;
}

int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	int i = 0;
	while (first)
	{
		printf("Node %d -->>: %d\n", i, first->info);
		first = first->next;
		i++;
	}		
	printf("%s : End\n",__func__);
	return 0;
}

Node* reverse_list(Node **start)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *next, *current, *prev;
	prev = NULL;
	current = (*start);				// address of start node
	while (current)
	{
		next = current->next;			// store next node address for iteration
		current->next = prev;			// prev from last node
		prev = current;				// store current node as prev for next
		current = next;				// move to next node
	}

	printf("%s : End\n",__func__);
	return prev;
}
