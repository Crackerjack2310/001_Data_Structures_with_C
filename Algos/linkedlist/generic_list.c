#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int display_list(Node *);

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *start;
	start = (Node *)malloc(sizeof(Node));

	get_list(start);			// input list
	display_list(start);			// display list
	
	printf("%s : End\n",__func__);
	return 0;
}
int get_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	first->info = 0;
	for (i = 1; i < 10; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = NULL;
	printf("%s : End\n",__func__);
	return 0;
}
int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	int i = 0;
	while (first)
	{
		printf("Node %d -->>: %f\n", i, (float)(first->info));
		first = first->next;
		i++;
	}		
	printf("%s : End\n",__func__);
	return 0;
}
