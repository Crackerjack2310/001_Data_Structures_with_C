#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int display_list(Node *);
Node* reverse_list(Node *);

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *start, *last;
	start = (Node *)malloc(sizeof(Node));
	last = start;
	get_list(start);				// input list
	printf("Now displaying list\n");
	while (last->next)
		last = last->next;
	display_list(start);				// display list
	reverse_list(start);
	start->next = NULL;
	printf("Now displaying reversed list\n");
	display_list(last);				// display list

	printf("%s : End\n",__func__);
	return 0;
}

int get_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	first->info = 0;
	for (i = 1; i < 10; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = NULL;
	printf("%s : End\n",__func__);
	return 0;
}

int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	int i = 0;
	while (first)
	{
		printf("Node %d -->>: %d\n", i, first->info);
		first = first->next;
		i++;
	}		
	printf("%s : End\n",__func__);
	return 0;
}

Node* reverse_list(Node *rest)		// as address of start
{
	printf("%s : Begin\n",__func__);
	Node *new;
	
	if (!rest->next)
		return rest;
	new = reverse_list(rest->next);
	new->next = rest;
	printf("%s : End\n",__func__);
	return rest;
}
