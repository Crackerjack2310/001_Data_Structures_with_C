/* The Algorithm used is called Floyd's cycle detection or Hare and the tortoise algorithm */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node *next;
	int info;
}Node;

int get_list(Node *);
int detect_loop(Node *);
int detect_start_of_loop(Node *, Node *);
int remove_loop(Node *, Node *);
int display_list(Node*);

int main()
{
	printf("%s : Begin\n",__func__);	
	Node *start;
	start = (Node *)malloc(sizeof(Node));
	int choice;
	get_list(start);
	display_list(start);

loop:	detect_loop(start);
	printf("Check again ?? 	Yes{0} No{1}\n");
	scanf("%d", &choice);
	if (!choice)
		goto loop;	

	printf("%s : End\n",__func__);
	return 0;
}

int get_list(Node *start)
{
	printf("%s : Begin\n",__func__);
	
	int i;
	Node *first = start;
	first->info = 0;
	for (i = 1; i <= 10; i++)
	{
		first->next = (Node*)malloc(sizeof(Node));
		first->next->info = i;	
		first = first->next;			
	}	
	first->next = start->next->next->next->next;		// loop condition
	printf("%s : End\n",__func__);
	return 0;
}
int display_list(Node *first)
{
	printf("%s : Begin\n",__func__);
	for (int i = 0; i <= 10; i++)
	{
		printf("Node %d : %d\n", i, first->info);
		first = first->next;
	}		
	printf("%s : End\n",__func__);
	return 0;
}

int detect_loop(Node *start)
{
	printf("%s : Begin\n",__func__);
	
	Node *h, *t;
	h = t = start;
	while (t)	
	{
		h = h->next;
		
		if (!t->next)
			break;
		t = t->next->next;
		
		if (h == t)			
		{
			printf("Loop detected\n");
			detect_start_of_loop(start, h);
			return 0;
		}
	}
	printf("No Loop \n");
	printf("%s : End\n",__func__);
	return 0;
}

int detect_start_of_loop(Node *start, Node* h)
{
	printf("%s : Begin\n",__func__);
	
	Node *t = start;
	int i = 1;
	while (1)
	{
		if (h == t)
		{
			printf("Loop starts at node %d\n", i);
			remove_loop(start,h);
			return 0;
		}
		i++;		
		h = h->next;
		t = t->next;
	}	
}

int remove_loop(Node *start, Node* loop_start)
{
	printf("%s : Begin\n",__func__);
	
	Node *t = start;
	int i = 0;
	
/*	while (1)
	{
		if ((t->next == loop_start) && (i == 1))	// ignore first occurrence of loop_start address
		{
			t->next = NULL;
			printf("Loop removed\n");
			return 0;
		}
	
		if ((t->next == loop_start) && (i == 0))	// second occurence is the reason for the loop	
			i = 1;
	
		t = t->next;
	}
*/
	while (t->next != loop_start)
		t = t->next;			// loop till first occurence
	t = t->next;
	while (t->next != loop_start)		// loop till second and then NULL it
		t = t->next;
	t->next = NULL;				// loop removed
	return 0;
}	
