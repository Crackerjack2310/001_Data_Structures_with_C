#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
 
int main(){
 
        int file_descriptor,error_code;
        char filename[] = "sample_text.txt";
 
        file_descriptor = open(filename, O_WRONLY | O_CREAT | O_EXCL);
 
        if(file_descriptor == -1) {
                perror("cannot open file\n");
                exit(1);
                }
 
        /* close the opened file */
        error_code = close(file_descriptor);
 
        if(error_code == -1){
                perror("cannot close file\n");
                exit(1);
                }
 
        return 0;
}
