#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
	int fd,err;
        char ch;
	fd=open("myfile",O_RDONLY|O_CREAT);
	
	if (fd==-1)
	{
		perror("Open() failed");
		exit(EXIT_FAILURE);
	}
	while(read(fd,&ch,1))
	{
		if(ch==10)
		{
			break;
		}
		printf("%c",ch);
	
	
	}
	printf("\nFile Descriptor value: %d\n",fd);
	err=close(fd);
	printf("\nErr value = %d\n",err);

	return 0;

}
