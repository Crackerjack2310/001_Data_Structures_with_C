#include<stdio.h>
#include<fcntl.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
	int ofd,rfd,err;
        char ch;
	ofd=open("writefile",O_RDWR|O_CREAT);
	rfd=open("readfile",O_RDWR);
	
	if (ofd==-1)
	{
		perror("ofd failed");
		exit(EXIT_FAILURE);
	}
	
	if (rfd==-1)
	{
		perror("rfd failed");
		exit(EXIT_FAILURE);
	}
	
	while(read(rfd,&ch,1))
	{
		write(ofd,&ch,1);
		if(ch==10)
		{
			break;
		}
	//	printf("%c",ch);


	}
	printf("\nFile Descriptor value: %d  %d\n",ofd,rfd);
	close(ofd);
	close(rfd);

	return 0;
}
