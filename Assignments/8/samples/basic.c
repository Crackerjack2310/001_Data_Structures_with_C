#include<stdio.h>
#include<stdlib.h>

int main()
{
	int *p = (int *)malloc(24*sizeof(int));
	for (int i = 0; i < 24; i++)
		*(p + i) = i;
	
	for (int i = 0; i < 24; i++)
		printf("value : %d\n", *(p + i));		// address incremented after dereferenced = 0

	return 0;
}
