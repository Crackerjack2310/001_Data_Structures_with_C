#include<stdio.h>
#include<stdlib.h>

int main()
{
	int a = 12, b = 78;
	int *pa = &a, *pb = &b;
	int **ppa, **ppb;
	ppa = &pa;	
	ppb = &pb;	
	printf("**ppa : %d\n", **ppa);		// address incremented after dereferenced = 0
	ppa = &pb;
	printf("**ppa : %d\n", **ppa);		// address incremented after dereferenced = 0
	system("pause");
	return 0;
}
