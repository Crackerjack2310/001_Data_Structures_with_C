#include<stdio.h>
#include<stdlib.h>

int main()
{
	static char *s[] = {"black", "yellow", "green", "red"};
	char **ptr[] = {s+3, s+2, s+1, s}, ***p;
	p = ptr;		// p and ptr now point to the 1st array element
				// p now points to 2nd element of the ptr array
				// p holds address of (s + 2)
	p = p + 1;

	printf("Address : %s\n", **p+1);	
	return 0;
}

