#include<stdio.h>
#include<stdlib.h>

int main()
{
	int *a, *s, i;
	s = a = (int *)malloc(sizeof(int)*4);
	for (i = 0; i < 4; i++)
		*(a + i) = i;
	for (i = 0; i < 4; i++)
		printf("%d  %d\n", *(s+i), s+i);		 

	printf("\v%d\n", *s++);		// address incremented after dereferenced = 0
	printf("%d\n", *s);		// derefrencing the already updated address = 1  
	printf("%d\n", ++s);		// increment address
	printf("%d\n", *s++);		//  
	printf("%d\n\v", *s);		// 
	
	for (i = 0; i < 4; i++)
		printf("%d  %d\n", *(a+i), a+i);		 
	return 0;
}
